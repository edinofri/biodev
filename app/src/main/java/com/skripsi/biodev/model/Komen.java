package com.skripsi.biodev.model;

public class Komen {
    private Integer idkomen;
    private Integer userid;
    private Integer usertid;
    private Integer artikelid;
    private String komennya;
    private String tanggal;
    private String nama;
    private String email;

    public Integer getIdkomen() {
        return idkomen;
    }

    public void setIdkomen(Integer idkomen) {
        this.idkomen = idkomen;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getUsertid() {
        return usertid;
    }

    public void setUsertid(Integer usertid) {
        this.usertid = usertid;
    }

    public Integer getArtikelid() {
        return artikelid;
    }

    public void setArtikelid(Integer artikelid) {
        this.artikelid = artikelid;
    }

    public String getKomennya() {
        return komennya;
    }

    public void setKomennya(String komennya) {
        this.komennya = komennya;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
