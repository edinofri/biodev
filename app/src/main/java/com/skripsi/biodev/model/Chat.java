package com.skripsi.biodev.model;

/**
 * Created by tukangbasic on 24/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */

public class Chat {
    public int sender_id;
    public String sender_name;
    public int receiver_id;
    public String receiver_name;
    public String message;
    public String date;

    public Chat(int sender_id, String sender_name, int receiver_id, String receiver_name, String message, String date) {
        this.sender_id = sender_id;
        this.sender_name = sender_name;
        this.receiver_id = receiver_id;
        this.receiver_name = receiver_name;
        this.message = message;
        this.date = date;
    }

    public Chat() {
    }
}
