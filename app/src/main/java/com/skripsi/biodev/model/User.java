package com.skripsi.biodev.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class User extends MessageResponse implements Parcelable {

    private UserData data;
    private List<Jurnal> pictures = null;
    private List<Subcategory> subcategories;
    private User user;

    protected User(Parcel in) {
        if (in.readByte() == 0x01) {
            pictures = new ArrayList<>();
            in.readList(pictures, Jurnal.class.getClassLoader());
        } else {
            pictures = null;
        }
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    public List<Jurnal> getPictures() {
        return pictures;
    }

    public void setPictures(List<Jurnal> pictures) {
        this.pictures = pictures;
    }

    public List<Subcategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<Subcategory> categories) {
        this.subcategories = subcategories;
    }

//	public User getUserData() {
//		return user
//	}
//
//	public void setJurnal(Jurnal Jurnal) {
//		this.user = user;
//	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(pictures);
    }
}
