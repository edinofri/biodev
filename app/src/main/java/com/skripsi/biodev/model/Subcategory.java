package com.skripsi.biodev.model;

public class Subcategory {
    private String kategori;
    private String subkategori;

    public Subcategory(String kategori, String subkategori){
        this.kategori = kategori;
        this.subkategori = subkategori;
    }

    public Subcategory(){
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getSubkategori() {
        return subkategori;
    }

    public void setSubkategori(String subkategori) {
        this.subkategori = subkategori;
    }

}
