package com.skripsi.biodev.model;

import java.util.List;

/**
 * Created by tukangbasic on 23/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */

public class KomenResponse extends MessageResponse {
    public List<Komen> komen;
}
