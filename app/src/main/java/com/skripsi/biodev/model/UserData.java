package com.skripsi.biodev.model;

public class UserData{
    private int iduser;
    private String namadpn;
    private String namablkg;
    private String email;
    private String tgl;
    private String gender;
    private String bio;
    private int id_prov;
    private String alamat;
    private String nohp;
    private String notlp;
    private String gambar;
    private String poin;

    public UserData(){
    }

    public void setIdUser(int idUser){
        this.iduser = idUser;
    }

    public int getIdUser(){
        return iduser;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getEmail(){
        return email;
    }

    public String getNamed() {
        return namadpn;
    }

    public void setNamed(String namadpn) {
        this.namadpn = namadpn;
    }

    public String getNameb() {
        return namablkg;
    }

    public void setNameb(String namablkg) {
        this.namablkg = namablkg;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public int getIdProv() {
        return id_prov;
    }

    public void setIdProv(int id_prov) {
        this.id_prov = id_prov;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoTelp() {
        return notlp;
    }

    public void setNoTelp(String notlp) {
        this.notlp = notlp;
    }

    public String getNoHp() {
        return nohp;
    }

    public void setNoHp(String nohp) {
        this.nohp = nohp;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getPoin() {
        return poin;
    }

    public void setPoin(String poin) {
        this.poin = poin;
    }
}
