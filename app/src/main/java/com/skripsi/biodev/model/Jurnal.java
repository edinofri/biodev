package com.skripsi.biodev.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Jurnal implements Parcelable {
    private Integer idartikel;
    private String kategori;
    private String subkategori;
    private String permalink;
    private String judul;
    private String namaLatin;
    private String spesies;
    private String habitat;
    private String distribusi;
    private String manfaat;
    private String isi;
    private String tglpublish;
    private Integer publisher;
    private String tgledit;
    private Integer likes;
    private Integer views;
    private Integer share;
    private String thumbnail;

    public UserData getPublisher() {
        return publisher_data;
    }

    private UserData publisher_data;

    public Integer getIdartikel() {
        return idartikel;
    }

    public void setIdartikel(Integer idartikel) {
        this.idartikel = idartikel;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getSubkategori() {
        return subkategori;
    }

    public void setSubkategori(String subkategori) {
        this.subkategori = subkategori;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getNamaLatin() {
        return namaLatin;
    }

    public void setNamaLatin(String namaLatin) {
        this.namaLatin = namaLatin;
    }

    public String getSpesies() {
        return spesies;
    }

    public void setSpesies(String species) {
        this.spesies = spesies;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setDusun(String habitat) {
        this.habitat = habitat;
    }

    public String getDistribusi() {
        return distribusi;
    }

    public void setDistribusi(String distribusi) {
        this.distribusi = distribusi;
    }

    public String getManfaat() {
        return manfaat;
    }

    public void setManfaat(String manfaat) {
        this.manfaat = manfaat;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getTglpublish() {
        return tglpublish;
    }

    public void setTglpublish(String tglpublish) {
        this.tglpublish = tglpublish;
    }

    public Integer getPublisherId() {
        return publisher;
    }

    public void setPublisher(Integer publisher) {
        this.publisher = publisher;
    }

    public String getTgledit() {
        return tgledit;
    }

    public void setTgledit(String website) {
        this.tgledit = tgledit;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Integer getShare() {
        return share;
    }

    public void setShare(Integer share) {
        this.share = share;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }


    public Jurnal() {
    }

    public Jurnal(Integer idartikel,
                  String kategori,
                  String subkategori,
                  String permalink,
                  String judul,
                  String namaLatin,
                  String spesies,
                  String habitat,
                  String distribusi,
                  String manfaat,
                  String isi,
                  String tglpublish,
                  Integer publisher,
                  Integer likes,
                  Integer views,
                  Integer share, String thumbnail) {
        this.idartikel = idartikel;
        this.kategori = kategori;
        this.subkategori = subkategori;
        this.permalink = permalink;
        this.judul = judul;
        this.namaLatin = namaLatin;
        this.spesies = spesies;
        this.habitat = habitat;
        this.distribusi = distribusi;
        this.manfaat = manfaat;
        this.isi = isi;
        this.tglpublish = tglpublish;
        this.publisher = publisher;
        this.likes = likes;
        this.views = views;
        this.share = share;
        this.thumbnail = thumbnail;
    }

    protected Jurnal(Parcel in) {
        kategori = in.readString();
        subkategori = in.readString();
        permalink = in.readString();
        judul = in.readString();
        namaLatin = in.readString();
        spesies = in.readString();
        habitat = in.readString();
        distribusi = in.readString();
        manfaat = in.readString();
        isi = in.readString();
        tglpublish = in.readString();
        tgledit = in.readString();
        thumbnail = in.readString();
    }

    public static final Creator<Jurnal> CREATOR = new Creator<Jurnal>() {
        @Override
        public Jurnal createFromParcel(Parcel in) {
            return new Jurnal(in);
        }

        @Override
        public Jurnal[] newArray(int size) {
            return new Jurnal[size];
        }
    };


    public void setValues(Jurnal newPicture) {
        kategori = newPicture.kategori;
        subkategori = newPicture.subkategori;
        permalink = newPicture.permalink;
        judul = newPicture.judul;
        namaLatin = newPicture.namaLatin;
        spesies = newPicture.spesies;
        habitat = newPicture.habitat;
        judul = newPicture.judul;
        distribusi = newPicture.distribusi;
        manfaat = newPicture.manfaat;
        isi = newPicture.isi;
        tglpublish = newPicture.tglpublish;
        tgledit = newPicture.tgledit;
        thumbnail = newPicture.thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(kategori);
        parcel.writeString(subkategori);
        parcel.writeString(permalink);
        parcel.writeString(judul);
        parcel.writeString(namaLatin);
        parcel.writeString(spesies);
        parcel.writeString(habitat);
        parcel.writeString(distribusi);
        parcel.writeString(manfaat);
        parcel.writeString(isi);
        parcel.writeString(tglpublish);
        parcel.writeString(tgledit);
        parcel.writeString(thumbnail);
    }
}
