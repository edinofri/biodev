package com.skripsi.biodev.model;

/**
 * Created by tukangbasic on 24/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */

public class RoomChat {

    public String name;
    public String roomId;
    public String lastMessage;
    public int target_id;
}
