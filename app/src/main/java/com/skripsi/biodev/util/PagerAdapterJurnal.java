package com.skripsi.biodev.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.skripsi.biodev.fragment.JurnalTabFragment1;
import com.skripsi.biodev.fragment.JurnalTabFragment2;

public class PagerAdapterJurnal extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterJurnal(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                JurnalTabFragment1 tab1 = new JurnalTabFragment1();
                return tab1;
            case 1:
                JurnalTabFragment2 tab2 = new JurnalTabFragment2();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}