package com.skripsi.biodev.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;

import com.skripsi.biodev.BuildConfig;

public class GPSLocationReceiver {

    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";

    public static BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    if (BuildConfig.DEBUG)
                        Log.e("About GPS", "GPS is Enabled in your device");
                } else {
                    //If GPS turned OFF show Location Dialog
//                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    // showSettingDialog();
                    if (BuildConfig.DEBUG)
                        Log.e("About GPS", "GPS is Disabled in your device");
                }

            }
        }
    };
}