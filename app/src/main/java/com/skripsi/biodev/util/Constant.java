package com.skripsi.biodev.util;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import com.skripsi.biodev.BuildConfig;

public class Constant {
    public static final String BASE_URL = "http://biodev.karizal.xyz/index.php/";

    public static final String URL_PICTURES = "http://www.biodiversitywarriors.org/images/";

    public static final int SPLASH_DELAY = 3000;

    public static String getDate(){
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(c.getTime());

        if (BuildConfig.DEBUG)
            Log.d("Current time => ", date);

        return date;
    }

    public static String getDateTime(){
        Calendar c = Calendar.getInstance();

        SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.mmm");
        String dateTime = d.format(c.getTime());

        if (BuildConfig.DEBUG)
            Log.d("Current time => ", dateTime);

        return dateTime;
    }

    public static String uniqueID() {
        return UUID.randomUUID().toString();
    }
}
