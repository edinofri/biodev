package com.skripsi.biodev;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.Window;
import android.view.WindowManager;

import com.skripsi.biodev.MainActivity;
import com.skripsi.biodev.R;
import com.daimajia.androidanimations.library.Techniques;

import com.skripsi.biodev.util.PermissionHelperStorage;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import com.skripsi.biodev.MainActivity;
import com.skripsi.biodev.R;
import com.skripsi.biodev.util.Constant;
import com.skripsi.biodev.util.PermissionHelperLoc;
import com.skripsi.biodev.util.SessionManager;

import static com.skripsi.biodev.SignInActivity.USER_SESSION;

public class WelcomeActivity extends Activity {

    private static int splashInterval = 2000;
    PermissionHelperStorage permissionHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        permissionHelper = new PermissionHelperStorage(this);

        checkAndRequestPermissions();

        setContentView(R.layout.activity_welcome);

    }


    private boolean checkAndRequestPermissions() {
        permissionHelper.permissionListener(new PermissionHelperStorage.PermissionListener() {
            @Override
            public void onPermissionCheckDone() {
//                Intent intent = new Intent(Splash_splash.this, MainActivity.class);
                Intent intent = new Intent(WelcomeActivity.this, SignInActivity.class);
                startActivity(intent);
//                int splashInterval = Splash_splash.splashInterval;
//                splashInterval
                finish();
            }
        });

        permissionHelper.checkAndRequestPermissionStorage();

        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHelper.onRequestCallBack(requestCode, permissions, grantResults);
    }

}
