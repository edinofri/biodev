package com.skripsi.biodev.fragment;

import android.*;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.medialablk.easytoast.EasyToast;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.skripsi.biodev.BuildConfig;
import com.skripsi.biodev.R;
import com.skripsi.biodev.model.User;
import com.skripsi.biodev.network.APIService;
import com.skripsi.biodev.util.SessionManager;
import com.skripsi.biodev.SignInActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.skripsi.biodev.MainActivity.iduser;
import static com.skripsi.biodev.util.Constant.URL_PICTURES;
import static com.skripsi.biodev.SignInActivity.USER_SESSION;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    @BindView(R.id.user_profile_name)TextView mTextProfileName;
    @BindView(R.id.text_view_address)TextView mTextProfileAddress;
    @BindView(R.id.text_view_user_email)TextView mTextProfileEmail;

    public String[] mColors = {
            "#39add1", // light blue
            "#3079ab", // dark blue
            "#c25975", // mauve
            "#e15258", // red
            "#f9845b", // orange
            "#838cc7", // lavender
            "#7d669e", // purple
            "#53bbb4", // aqua
            "#51b46d", // green
            "#e0ab18", // mustard
            "#637a91", // dark gray
            "#f092b0", // pink
            "#b7c0c7"  // light gray
    };

    private GoogleMap mGoogleMap;
    private View mView;
    private MapView mMapView;

    private Boolean[] markerD;
    private String[] urlFoto;
    private Double[] latitude, longitude;
    private LatLng[] latLng;
    private String namaUser;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_profile, container, false);

        ButterKnife.bind(this, mView);

        initComponents();

        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeMap();
    }

    private void initializeMap() {
        mMapView = mView.findViewById(R.id.map);
        if (mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
        }
    }

    private void initComponents(){
        setHasOptionsMenu(true);

        User user = SessionManager.getUser(getActivity(), USER_SESSION);
        if (user != null){
            mTextProfileName.setText(user.getData().getNamed());
            mTextProfileAddress.setText(user.getData().getAlamat());
            mTextProfileEmail.setText(user.getData().getEmail());

        }

        User sekolah = SessionManager.getUser(getActivity(), "user_session");
        if (sekolah != null){
            mTextProfileName.setText(sekolah.getData().getNamed());
        }
    }

    private void logout(){
        new AlertDialog.Builder(getActivity())
                .setMessage("Apakah anda yakin ingin logout?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SessionManager.clear(getActivity());
                        startActivity(new Intent(getActivity(), SignInActivity.class));
                        getActivity().finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    public int getColor() {
        String color;

        // Randomly select a fact
        Random randomGenerator = new Random(); // Construct a new Random number generator
        int randomNumber = randomGenerator.nextInt(mColors.length);

        color = mColors[randomNumber];
        int colorAsInt = Color.parseColor(color);

        return colorAsInt;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.options, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.menu_about) {
//            FragmentManager fm = getActivity().getFragmentManager();
//            fm.beginTransaction().replace(R.id.content, new AboutFragment()).addToBackStack(null).commit();
//            return true;
//        } else
        if (id == R.id.menu_logout){
            logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadDataLembaga() {
        APIService apiService = new APIService();
        apiService.getUser(iduser, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                User user = (User) response.body();
                User user1 = SessionManager.getUser(getActivity(), "user");
                if (user != null) {
                    if (!user.isError()) {
                        int size = user.getPictures().size();

                        markerD = new Boolean[size];
                        urlFoto = new String[size];
                        latitude = new Double[size];
                        longitude = new Double[size];
                        latLng = new LatLng[size];

                        for (int i = 0; i < size; i++){
                            urlFoto[i] = user.getPictures().get(i).getThumbnail();

                            latLng[i] = new LatLng(latitude[i], longitude[i]);
                            markerD[i] = false;
                            Glide.with(getActivity())
                                    .load(URL_PICTURES + urlFoto[i])
                                    .into(new SimpleTarget<Drawable>() {
                                        @Override
                                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {

                                        }
                                    });

                            if (user1 != null){
                                namaUser = user1.getData().getNamed();
                            }

                            mGoogleMap.addMarker(new MarkerOptions()
                                    .position(latLng[i])
                                    .title(namaUser)

                            );

                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng[i], 12.0f));
                        }
                    } else {
                        if (BuildConfig.DEBUG)
                            Log.d("TAG", user.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (BuildConfig.DEBUG)
                    Log.e("TAG", t.getMessage());
                Log.d("HOME" + " -> onFailure: ", t.getMessage());

                EasyToast.error(getActivity(), "Connection error");
            }
        });
    }
}

