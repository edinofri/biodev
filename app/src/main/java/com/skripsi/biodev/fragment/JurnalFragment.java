package com.skripsi.biodev.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.medialablk.easytoast.EasyToast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

import com.skripsi.biodev.App;
import com.skripsi.biodev.BuildConfig;
import com.skripsi.biodev.R;
import com.skripsi.biodev.adapter.JurnalAdapter;
import com.skripsi.biodev.adapter.PhotoAdapter;
import com.skripsi.biodev.model.MessageResponse;
import com.skripsi.biodev.model.Jurnal;
import com.skripsi.biodev.model.User;
import com.skripsi.biodev.network.APIService;
import com.skripsi.biodev.util.BitmapUtils;
import com.skripsi.biodev.util.NetworkUtils;
import com.skripsi.biodev.util.PermissionHelperStorage;

import static com.skripsi.biodev.MainActivity.iduser;
import static com.skripsi.biodev.util.ViewUtils.numberOfColumns;

/**
 * A simple {@link Fragment} subclass.
 */
public class JurnalFragment extends Fragment {
    @BindView(R.id.rv_photo)
    RecyclerView mRecyclerView;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefresh;

    static RelativeLayout mProgressLayout;
    static TextView mTextMessage;
    static RelativeLayout mErrorLayout;

    private LinearLayoutManager linearLayoutManager;

    private ProgressDialog progressDialog;

    private static APIService apiService;

    private static JurnalAdapter adapter;
    private static User user;

    static Activity activity = null;

    int page = 1;
    private boolean isEndPage = false;

    PermissionHelperStorage permissionHelper;

    public JurnalFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jurnal, container, false);

        ButterKnife.bind(this, view);

        permissionHelper = new PermissionHelperStorage(getActivity());

        activity = (Activity) view.getContext();

        initComponents(view);

        if (savedInstanceState != null) {
            mRecyclerView.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable("List-State"));
            adapter.setDataAdapter(Arrays.asList(App.getInstance().getGson().fromJson(savedInstanceState.getString("List-Data"), Jurnal[].class)));
        } else {
            page = 1;
            isEndPage = false;
            getDataPictures();
        }

        mRefresh.setColorSchemeColors(Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE);
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mRefresh.setRefreshing(false);
                page = 1;
                isEndPage = false;
                getDataPictures();
            }
        });
//        if (mNeedReload) getDataPictures();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        eventBus.unregister(this);
    }

    public boolean isInternetAvailable() {
        return NetworkUtils.isNetworkConnected(getActivity());
    }

    private void initComponents(View view) {

        mErrorLayout = view.findViewById(R.id.main_error_layout);
        mProgressLayout = view.findViewById(R.id.main_progress_layout);
        mTextMessage = view.findViewById(R.id.tv_message_display);

        progressDialog = new ProgressDialog(getActivity());

        mRecyclerView.setHasFixedSize(true);

        linearLayoutManager = new GridLayoutManager(getActivity(), numberOfColumns(getActivity()));
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new JurnalAdapter(getActivity());
        mRecyclerView.setAdapter(adapter);

        adapter.setOnLoadMoreListener(mRecyclerView, new PhotoAdapter.LoadMore() {
            @Override
            public void OnloadMoreListener() {
                if (!isEndPage) {
                    page++;
                    getDataPictures();
                }
            }
        });
    }

    private void getDataPictures() {
        if (isInternetAvailable()) {
            loadDataPictures();
        } else {
            Snackbar snackbar = Snackbar
                    .make(getActivity().findViewById(android.R.id.content), R.string.no_internet, Snackbar.LENGTH_INDEFINITE)
                    .setAction("COBA LAGI", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getDataPictures();
                        }
                    });
            snackbar.setActionTextColor(Color.RED);

            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    public void loadDataPictures() {
        mProgressLayout.setVisibility(View.VISIBLE);
        apiService = new APIService();
        apiService.getDataSubcategory(page, "Jurnal", "Tumbuhan", new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mProgressLayout.setVisibility(View.GONE);
                user = (User) response.body();
                if (user != null) {
                    if (!user.isError()) {
                        isEndPage = false;
                        if (page == 1) {
                            adapter.setDataAdapter(user.getPictures());
                        } else {
                            adapter.addDataAdapter(user.getPictures());
                        }
//                        progressBar.setVisibility(View.GONE);
//                        adapter.addAll(results);
//
//                        if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
//                        else isLastPage = true;
                    } else {
                        isEndPage = true;
                        if (page == 1) {
                            mErrorLayout.setVisibility(View.VISIBLE);
                            mTextMessage.setText(user.getMessage());
                        }
                        if (BuildConfig.DEBUG)
                            Log.d("TAG", user.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                isEndPage = true;
                if (BuildConfig.DEBUG)
                    Log.e("TAG", t.getMessage());
                Log.d("HOME" + " -> onFailure: ", t.getMessage());
                EasyToast.error(activity, "Koneksi eror");
                mProgressLayout.setVisibility(View.GONE);
                EasyToast.info(activity, "Tarik ke bawah untuk me-refresh", Toast.LENGTH_LONG);
            }
        });
    }
}