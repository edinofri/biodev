package com.skripsi.biodev.fragment;

import android.app.Activity;
import android.content.Intent;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medialablk.easytoast.EasyToast;
import com.skripsi.biodev.BuildConfig;
import com.skripsi.biodev.ChatActivity;
import com.skripsi.biodev.KomentarActivity;
import com.skripsi.biodev.R;

import android.widget.ImageView;
import android.widget.TextView;

import com.skripsi.biodev.adapter.KomentarAdapter;
import com.skripsi.biodev.model.Jurnal;
import com.skripsi.biodev.model.Komen;
import com.skripsi.biodev.model.KomenResponse;
import com.skripsi.biodev.network.config.ApiClient;
import com.skripsi.biodev.network.config.ApiInterface;
import com.google.gson.Gson;

//import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link android.app.Fragment} subclass.
 */
public class JurnalDetFragment extends Fragment {

    private static final int REQUEST_COMMENT = 1001;
    @BindView(R.id.txtJudul)
    TextView txtJudul;
    @BindView(R.id.txtPublisher)
    TextView txtPublisher;
    @BindView(R.id.txtTglPublish)
    TextView txtTglPublish;
    @BindView(R.id.txtSumber)
    TextView txtSumber;
    @BindView(R.id.IsiText)
    TextView IsiText;

    @BindView(R.id.image_view)
    ImageView image_view;


    @BindView(R.id.komentar_label)
    TextView txtLabelKomentar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

//    @BindView(R.id.img_signature)
//    ImageView imgSignature;
//    Bitmap bitmap;

    private Jurnal itemJurnalList;

    String ListJurnal;

    private ApiInterface apiInterface;
    private List<Komen> comments;
    private KomentarAdapter adapter;

    public JurnalDetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jurnal_det, container, false);

        ButterKnife.bind(this, view);


        bindData();

        itemJurnalList.getIdartikel();

        txtJudul.setText(itemJurnalList.getJudul());
        txtPublisher.setText(itemJurnalList.getPublisher().getNamed());
        txtSumber.setText(itemJurnalList.getSubkategori());
        IsiText.setText(itemJurnalList.getIsi());
        txtTglPublish.setText(itemJurnalList.getTglpublish());

        //txtStatusPOD.setText(itemHistoryList.getStatus_pod().toString());

        adapter = new KomentarAdapter();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        apiInterface = ApiClient.builder()
                .create(ApiInterface.class);

        getComments();
        return view;
    }

    private void getComments() {
        apiInterface.getComments(itemJurnalList.getIdartikel()).enqueue(new Callback<KomenResponse>() {
            @Override
            public void onResponse(Call<KomenResponse> call, Response<KomenResponse> response) {
                KomenResponse komenResponse = response.body();
                if (komenResponse != null) {
                    if (!komenResponse.isError()) {
                        EasyToast.success(getActivity(), komenResponse.getMessage());
                        setComments(komenResponse.komen);
                    } else {
                        setComments(new ArrayList());
                        EasyToast.error(getActivity(), komenResponse.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<KomenResponse> call, Throwable t) {
                if (BuildConfig.DEBUG) {
                    Log.d("HOME" + " -> onFailure: ", t.getMessage());
                }
                EasyToast.error(getActivity(), "Koneksi eror");
            }
        });
    }


    private void bindData() {
        if (getArguments() != null) {
            //ini yang buat get datanya yang dari adapter tadi
            ListJurnal = getArguments().getString("Jurnal");
        }

        itemJurnalList = new Gson().fromJson(ListJurnal, Jurnal.class);
    }

    public void setComments(List<Komen> comments) {
        this.comments = comments;
        adapter.setItems(comments);
    }

    @OnClick(R.id.btn_add_komentar)
    public void addComment() {
        Intent intent = new Intent(getActivity(), KomentarActivity.class);
        intent.putExtra("data", new Gson().toJson(itemJurnalList));
        startActivityForResult(intent, REQUEST_COMMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_COMMENT) {
            getComments();
        }
    }

    @OnClick(R.id.txtPublisher)
    public void openChat() {
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.putExtra("receiver_id", itemJurnalList.getPublisherId());
        intent.putExtra("receiver_name", itemJurnalList.getPublisher().getNamed());
        startActivity(intent);
    }
}
