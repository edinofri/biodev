package com.skripsi.biodev;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Base64;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.support.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.skripsi.biodev.adapter.PhotoAdapter;
import com.skripsi.biodev.network.APIService;
import com.skripsi.biodev.model.Jurnal;
import com.skripsi.biodev.model.MessageResponse;
import com.skripsi.biodev.model.User;
import com.skripsi.biodev.network.config.ApiClient;
import com.skripsi.biodev.util.BitmapUtils;
import com.skripsi.biodev.util.MyLocation;
import com.skripsi.biodev.util.PermissionHelperStorage;
import com.skripsi.biodev.util.RequestBodyHelper;
import com.skripsi.biodev.util.SessionManager;
import com.skripsi.biodev.network.config.ApiInterface;
import com.google.android.gms.vision.text.Line;
import com.google.gson.Gson;
import com.medialablk.easytoast.EasyToast;

import android.view.View;
import android.widget.Toast;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.echodev.resizer.Resizer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

import static com.skripsi.biodev.util.Constant.uniqueID;
import static android.app.Activity.RESULT_OK;
import static com.skripsi.biodev.MainActivity.iduser;
import static com.skripsi.biodev.util.Constant.getDate;
import static com.skripsi.biodev.util.Constant.getDateTime;
import static com.skripsi.biodev.util.Constant.uniqueID;
import static com.skripsi.biodev.util.RealPathUtils.getRealPathFromURI_API19;
import static com.skripsi.biodev.util.RealPathUtils.getRealPathFromUri;
import static com.skripsi.biodev.util.ViewUtils.numberOfColumns;

import com.skripsi.biodev.util.BitmapUtils;
import com.skripsi.biodev.util.NetworkUtils;
import com.skripsi.biodev.util.PermissionHelperStorage;

public class PostingActivity extends AppCompatActivity {

    @BindView(R.id.category)
    Spinner spnCategory;
    @BindView(R.id.subcategory)
    Spinner spnSubCategory;
    @BindView(R.id.judul)
    EditText txtJudul;
    @BindView(R.id.namalatin)
    TextView txtNamalatin;
    @BindView(R.id.species)
    TextView txtSpecies;
    @BindView(R.id.habitat)
    TextView txtHabitat;
    @BindView(R.id.distribusi)
    TextView txtDistribusi;
    @BindView(R.id.manfaat)
    TextView txtManfaat;
    @BindView(R.id.Isi)
    TextView txtIsi;
    @BindView(R.id.image_viewpost)
    ImageView image_viewpost;
    @BindView(R.id.tvCamera)
    TextView tvCameraa;
    @BindView(R.id.layout_foto)
    FrameLayout layout_foto;
    @BindView(R.id.save_button)
    Button btnSave;


    @BindView(R.id.btn_find_location)
    LinearLayout btnFindLocation;
    @BindView(R.id.btn_find_location_text)
    TextView btnFindLocationText;
    @BindView(R.id.location)
    TextView txtLocation;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    private ProgressDialog progressDialog;

    private String kategori;
    private String subkategori;
    private String latitude;
    private String longitude;
    private boolean isGPSSearching = false;
    private boolean isCoarseLocationGrant;
    private boolean isFineLocationGrant;

    private ArrayList<File> mFile = new ArrayList<>();

    private MyLocation myLocation = new MyLocation();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posting);
        ButterKnife.bind(this);

        initComponents();
    }

    private void initComponents() {
        Nammu.init(this);
        initPermission();
        initImagePicker();
        getDate();
        loadDataCategories();
        loadDataSubcategories();

        progressDialog = new ProgressDialog(PostingActivity.this);
    }

    private void loadDataCategories() {
        final String[] item = {"Jurnal", "Katalog"};
        spnCategory.setAdapter(new ArrayAdapter<String>(PostingActivity.this,
                android.R.layout.simple_spinner_dropdown_item, item));
        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                if (spnCategory.getSelectedItem().toString().equals(item)) {
                    kategori = item[i];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadDataSubcategories() {
        final String[] item = {"Hewan", "Tumbuhan",
                "Jamur", "Pangan",
                "Energi", "Kesehatan",
                "Air", "Lain-lain"};
        spnSubCategory.setAdapter(new ArrayAdapter<String>(PostingActivity.this,
                android.R.layout.simple_spinner_dropdown_item, item));

        spnSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spnSubCategory.getSelectedItem().equals(item)) {
                    subkategori = item[i];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                onPhotosReturned(imageFiles);
            }

            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                e.printStackTrace();
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(PostingActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    private void onPhotosReturned(List<File> returnedPhotos) {
        mFile.addAll(returnedPhotos);
        Glide.with(this).load(returnedPhotos.get(0).getAbsolutePath()).into(image_viewpost);
    }

    @OnClick(R.id.save_button)
    public void saveMe(View view) {
        // Upload the image
        insertJurnal();
    }

    private void insertJurnal() {
        ApiInterface apiInterface = ApiClient.builder()
                .create(ApiInterface.class);

        String Judul = txtJudul.getText().toString();
        String Namalatin = txtNamalatin.getText().toString();
        String Species = txtSpecies.getText().toString();
        String Habitat = txtHabitat.getText().toString();
        String Distribusi = txtDistribusi.getText().toString();
        String Manfaat = txtManfaat.getText().toString();
        String Keterangan = txtIsi.getText().toString();

        Map<String, RequestBody> data = new HashMap<>();
        data.put("kategori", RequestBodyHelper.with(this).convertToRequestBody(kategori));
        data.put("subkategori", RequestBodyHelper.with(this).convertToRequestBody(subkategori));
        data.put("judul", RequestBodyHelper.with(this).convertToRequestBody(Judul));
        data.put("namaLatin", RequestBodyHelper.with(this).convertToRequestBody(Namalatin));
        data.put("spesies", RequestBodyHelper.with(this).convertToRequestBody(Species));
        data.put("habitat", RequestBodyHelper.with(this).convertToRequestBody(Habitat));
        data.put("distribusi", RequestBodyHelper.with(this).convertToRequestBody(Distribusi));
        data.put("manfaat", RequestBodyHelper.with(this).convertToRequestBody(Manfaat));
        data.put("isi", RequestBodyHelper.with(this).convertToRequestBody(Keterangan));
        data.put("tglpublish", RequestBodyHelper.with(this).convertToRequestBody(getDate()));
        data.put("publisher", RequestBodyHelper.with(this).convertToRequestBody(String.valueOf(iduser)));
        data.put("latitude", RequestBodyHelper.with(this).convertToRequestBody(String.valueOf(latitude)));
        data.put("longitude", RequestBodyHelper.with(this).convertToRequestBody(String.valueOf(longitude)));

        // special case for multipart
        MultipartBody.Part image = null;
        if (mFile.size() > 0) {
            try {
                File file = new Resizer(this)
                        .setTargetLength(1080)
                        .setQuality(30)
                        .setOutputFormat("JPEG")
                        .setSourceImage(mFile.get(0))
                        .getResizedFile();

                RequestBody thumbnail = RequestBody.create(MediaType.parse("image/jpeg"), file);
                image = MultipartBody.Part.createFormData("Thumbnail", file.getName(), thumbnail);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        showProgress();

        apiInterface.insertJurnal(image,data).enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                hideProgress();
                MessageResponse messageResponse = response.body();
                if (messageResponse != null) {
                    if (!messageResponse.isError()) {
                        EasyToast.success(PostingActivity.this, messageResponse.getMessage());
                        finish();
                    } else {
                        EasyToast.error(PostingActivity.this, messageResponse.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                if (BuildConfig.DEBUG) {
                    Log.d("HOME" + " -> onFailure: ", t.getMessage());
                }
                EasyToast.error(PostingActivity.this, "Koneksi eror");
                hideProgress();
            }
        });

    }

    private void upload() {
        ApiInterface apiInterface = ApiClient.builder()
                .create(ApiInterface.class);

        String Judul = txtJudul.getText().toString();
        String Namalatin = txtNamalatin.getText().toString();
        String Species = txtSpecies.getText().toString();
        String Habitat = txtHabitat.getText().toString();
        String Distribusi = txtDistribusi.getText().toString();
        String Manfaat = txtManfaat.getText().toString();
        String Keterangan = txtIsi.getText().toString();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("kategori", kategori);
        builder.addFormDataPart("subkategori", subkategori);
        builder.addFormDataPart("judul", Judul);
        builder.addFormDataPart("namaLatin", Namalatin);
        builder.addFormDataPart("spesies", Species);
        builder.addFormDataPart("habitat", Habitat);
        builder.addFormDataPart("distribusi", Distribusi);
        builder.addFormDataPart("manfaat", Manfaat);
        builder.addFormDataPart("isi", Keterangan);
        builder.addFormDataPart("tglpublish", getDate());
        builder.addFormDataPart("publisher", String.valueOf(iduser));
        // Multiple Images
        for (int i = 0; i < mFile.size(); i++) {
            File file = new File(String.valueOf(mFile.get(i)));
            try {
                File resizedImage = new Resizer(this)
                        .setTargetLength(1080)
                        .setQuality(30)
                        .setOutputFormat("JPEG")
                        .setSourceImage(file)
                        .getResizedFile();
                RequestBody requestImage = RequestBody.create(MediaType.parse("multipart/form-data"), resizedImage);
                builder.addFormDataPart("Thumbnail", file.getName(), requestImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        showProgress();
        MultipartBody requestBody = builder.build();
        Call<MessageResponse> call = apiInterface.uploadJurnal(requestBody);
        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                hideProgress();
                MessageResponse messageResponse = response.body();
                if (messageResponse != null) {
                    if (!messageResponse.isError()) {
                        EasyToast.success(PostingActivity.this, messageResponse.getMessage());
                    } else {
                        EasyToast.error(PostingActivity.this, messageResponse.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                if (BuildConfig.DEBUG) {
                    Log.d("HOME" + " -> onFailure: ", t.getMessage());
                }
                EasyToast.error(PostingActivity.this, "Koneksi eror");
                hideProgress();
            }
        });

    }

    private void showProgress() {
        //creating and showing progress dialog
        progressDialog.setMessage("Mohon tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
    }

    private void hideProgress() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.camera)
    public void takePicture() {
        EasyImage.openCamera(PostingActivity.this, 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void initImagePicker() {
        EasyImage.configuration(this)
                .setImagesFolderName("Biodev")
                .setCopyTakenPhotosToPublicGalleryAppFolder(true)
                .setCopyPickedImagesToPublicGalleryAppFolder(true)
                .setAllowMultiplePickInGallery(false);
    }

    private void initPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Nammu.askForPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, new PermissionCallback() {
                @Override
                public void permissionGranted() {
                    //Nothing, this sample saves to Public gallery so it needs permission
                }

                @Override
                public void permissionRefused() {
                    finish();
                }
            });
        }
    }

    @OnClick(R.id.btn_find_location)
    public void getLocation() {
        if (!checkPermissionLocation()) {
            return;
        }

        if (!isGPSSearching) {
            isGPSSearching = true;
            progressBar.setVisibility(View.VISIBLE);
            btnFindLocationText.setVisibility(View.INVISIBLE);

            if (!myLocation.getLocation(this, locationResult)) {
                isGPSSearching = false;
                progressBar.setVisibility(View.GONE);
                btnFindLocationText.setVisibility(View.VISIBLE);
                Toast.makeText(this, "Can't get location", Toast.LENGTH_SHORT).show();

            }
        }
    }


    private Boolean checkPermissionLocation() {
        int permissionCheckCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionCheckFineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        isCoarseLocationGrant = permissionCheckCoarseLocation == PackageManager.PERMISSION_GRANTED;
        isFineLocationGrant = permissionCheckFineLocation == PackageManager.PERMISSION_GRANTED;
        if (permissionCheckCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            Nammu.askForPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION, new PermissionCallback() {
                @Override
                public void permissionGranted() {
                    //Nothing, this sample saves to Public gallery so it needs permission
                    isCoarseLocationGrant = true;
                }

                @Override
                public void permissionRefused() {
                    Toast.makeText(PostingActivity.this, "Permission Refuse, somefunctional will be disable", Toast.LENGTH_SHORT).show();
                    isCoarseLocationGrant = false;
                }
            });
        }

        if (permissionCheckFineLocation != PackageManager.PERMISSION_GRANTED) {
            Nammu.askForPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, new PermissionCallback() {
                @Override
                public void permissionGranted() {
                    //Nothing, this sample saves to Public gallery so it needs permission
                    isFineLocationGrant = true;
                }

                @Override
                public void permissionRefused() {
                    Toast.makeText(PostingActivity.this, "Permission Refuse, somefunctional will be disable", Toast.LENGTH_SHORT).show();
                    isFineLocationGrant = false;
                }
            });
        }

        return isCoarseLocationGrant && isFineLocationGrant;
    }

    private MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {
        @Override
        public void gotLocation(Location location) {

            final double latitude = location.getLatitude();
            final double longitude = location.getLongitude();
            isGPSSearching = false;

            PostingActivity.this.latitude = String.valueOf(latitude);
            PostingActivity.this.longitude = String.valueOf(longitude);

            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    progressBar.setVisibility(View.GONE);
                    btnFindLocationText.setVisibility(View.VISIBLE);

                    txtLocation.setText(latitude + "," + longitude);
                    Toast.makeText(PostingActivity.this, "Location Updated", Toast.LENGTH_SHORT).show();

                }
            });
        }
    };
}
