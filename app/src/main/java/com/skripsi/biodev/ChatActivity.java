package com.skripsi.biodev;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.skripsi.biodev.adapter.ChatAdapter;
import com.skripsi.biodev.model.Chat;
import com.skripsi.biodev.model.User;
import com.skripsi.biodev.util.SessionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.skripsi.biodev.SignInActivity.USER_SESSION;
import static com.skripsi.biodev.util.Constant.getDate;

public class ChatActivity extends AppCompatActivity {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.input_message)
    EditText inputMessage;

    ChatAdapter adapter;

    private int receiver_id;
    private int sender_id;

    FirebaseDatabase database;
    DatabaseReference ref;
    List<Chat> chatList = new ArrayList<>();
    private String sender_name;
    private String receiver_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);

        adapter = new ChatAdapter(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        User user = SessionManager.getUser(this, USER_SESSION);
        sender_id = user.getData().getIdUser();
        receiver_id = getIntent().getIntExtra("receiver_id", 0);

        sender_name = user.getData().getNamed();
        receiver_name = getIntent().getStringExtra("receiver_name");

        database = FirebaseDatabase.getInstance();
        ref = database.getReference("chat");

        realTimeList();
    }

    private void realTimeList() {
        DatabaseReference roomRef = ref.child(roomId());
        roomRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chatList.clear();
                for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {
                    Chat chat = chatSnapshot.getValue(Chat.class);
                    chatList.add(chat);
                }
                adapter.setItems(chatList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @OnClick(R.id.btn_send)
    public void sendMessage() {
        if (inputMessage.getText().length() == 0) {
            return;
        }
        DatabaseReference roomRef = ref.child(roomId());
        Chat chat = new Chat(sender_id, sender_name, receiver_id, receiver_name, inputMessage.getText().toString(), getDate());
        roomRef.push().setValue(chat);
        inputMessage.setText("");
    }

    public String roomId() {
        return sender_id > receiver_id ? sender_id + "-" + receiver_id : receiver_id + "-" + sender_id;
    }
}
