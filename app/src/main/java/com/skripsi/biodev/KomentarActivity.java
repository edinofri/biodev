package com.skripsi.biodev;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.google.gson.Gson;
import com.medialablk.easytoast.EasyToast;
import com.skripsi.biodev.model.Jurnal;
import com.skripsi.biodev.model.MessageResponse;
import com.skripsi.biodev.model.User;
import com.skripsi.biodev.network.config.ApiClient;
import com.skripsi.biodev.network.config.ApiInterface;
import com.skripsi.biodev.util.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.skripsi.biodev.SignInActivity.USER_SESSION;
import static com.skripsi.biodev.util.Constant.getDate;

public class KomentarActivity extends AppCompatActivity {

    @BindView(R.id.isi_komentar)
    EditText txtIsiKomentar;
    private ApiInterface apiInterface;

    private Jurnal jurnal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komentar);
        ButterKnife.bind(this);
        apiInterface = ApiClient.builder()
                .create(ApiInterface.class);
        jurnal = new Gson().fromJson(getIntent().getStringExtra("data"), Jurnal.class);

    }

    @OnClick(R.id.btn_send)
    public void sendComment() {
        User user = SessionManager.getUser(this, USER_SESSION);
        apiInterface.addComment(jurnal.getPublisherId(),
                                user.getData().getIdUser(),
                                jurnal.getIdartikel(),
                                txtIsiKomentar.getText().toString(),
                                getDate())
                .enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                MessageResponse komenResponse = response.body();
                if (komenResponse != null) {
                    if (!komenResponse.isError()) {
                        EasyToast.success(KomentarActivity.this, komenResponse.getMessage());
                        setResult(Activity.RESULT_OK);
                        finish();
                    } else {
                        EasyToast.error(KomentarActivity.this, komenResponse.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                if (BuildConfig.DEBUG) {
                    Log.d("HOME" + " -> onFailure: ", t.getMessage());
                }
                EasyToast.error(KomentarActivity.this, "Koneksi eror");
            }
        });
    }
}
