package com.skripsi.biodev.network.config;


import com.skripsi.biodev.model.KomenResponse;
import com.skripsi.biodev.model.MessageResponse;
import com.skripsi.biodev.model.User;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    Call<User> signIn(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("signup")
    Call<User> userSignUp(@Field("username") String username,
                          @Field("email") String email,
                          @Field("password") String password);

    @FormUrlEncoded
    @POST("insert-jurnal")
    Call<MessageResponse> uploadJurnal(
            @Body RequestBody file
    );

    @GET("user/{iduser}")
    Call<User> getDataUser(@Path("iduser") int iduser);


    @GET("jurnal/kategori/page/{halaman}")
    Call<User> getDataArtikel(@Path("halaman") Integer halaman,
                              @Query("kategori") String kategori);

    @GET("jurnal/subkategori/page/{halaman}")
    Call<User> getDataSubcategory(@Path("halaman") Integer halaman,
                                  @Query("kategori") String kategori,
                                  @Query("subkategori") String subkategori);
    ////

    @GET("jurnal/publisher/{publisher}")
    Call<User> getJurnalProfile(@Path("publisher") Integer publisher);

    @GET("category")
    Call<User> getDataCategory();

    @GET("poin")
    Call<User> getDataPoin();

    @GET("komen/{idkomen}/{artikelid}")
    Call<User> getKomen(@Path("idkomen") int idkomen,
                        @Path("artikelid") int artikelid);

    @GET("jurnal/komen/{article_id}")
    Call<KomenResponse> getComments(@Path("article_id") int article_id);

    @FormUrlEncoded
    @PUT("delete-pictures/{idartikel}")
    Call<User> deletePictures(@Path("idartikel") Integer idartikel,
                              @Field("Soft_delete") int soft_delete);

    @FormUrlEncoded
    @PUT("update-pictures/{idartikel}")
    Call<User> updatePictures(@Path("idartikel") int idartikel,
                              @Field("isi") String isi,
                              @Field("kategori") int kategori,
                              @Field("tgledit") String tgledit);

    @Multipart
    @POST("insert-jurnal")
    Call<MessageResponse> insertJurnal(
            @Part MultipartBody.Part file,
            @PartMap Map<String, RequestBody> data
    );
    @FormUrlEncoded
    @POST("komen")
    Call<MessageResponse> addComment(@Field("userid") int publisherId,
                                     @Field("usertid") int userId,
                                     @Field("artikelid") int articleId,
                                     @Field("komennya") String comment,
                                     @Field("tanggal") String date);
}
