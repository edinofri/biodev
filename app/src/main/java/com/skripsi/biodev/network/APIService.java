package com.skripsi.biodev.network;


import com.skripsi.biodev.network.config.ApiClient;
import com.skripsi.biodev.network.config.ApiInterface;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Callback;


public class APIService {
    private ApiInterface apiInterface;

    public APIService() {
        apiInterface = ApiClient.builder()
                .create(ApiInterface.class);
    }

    public void doDelete(Integer idartikel, int soft_delete, Callback callback) {
        apiInterface.deletePictures(idartikel, soft_delete).enqueue(callback);
    }

    public void doLogin(String username, String password, Callback callback) {
        apiInterface.signIn(username, password).enqueue(callback);
    }

    public void doRegister(String username, String email, String password, Callback callback) {
        apiInterface.userSignUp(username, email, password).enqueue(callback);
    }

    public void getDataArtikel(Integer halaman, String kategori, Callback callback) {
        apiInterface.getDataArtikel(halaman, kategori).enqueue(callback);
    }

    public void getDataSubcategory(Integer halaman, String kategori, String subkategori, Callback callback) {
        apiInterface.getDataSubcategory(halaman, kategori, subkategori).enqueue(callback);
    }

    public void getUser(int iduser, Callback callback) {
        apiInterface.getDataUser(iduser).enqueue(callback);
    }

    public void getJurnalProfile(Integer publisher, Callback callback) {
        apiInterface.getJurnalProfile(publisher).enqueue(callback);
    }

    public void doUpdate(int idartikel, String isi, int kategori, String tgledit, Callback callback) {
        apiInterface.updatePictures(idartikel, isi, kategori, tgledit).enqueue(callback);
    }

    public void doInsertJurnal(Map<String, RequestBody> data,Callback callback){
//        apiInterface.insertJurnal(,data).enqueue(callback);
    }


}
