package com.skripsi.biodev;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.reflect.TypeToken;
import com.skripsi.biodev.adapter.ListChatAdapter;
import com.skripsi.biodev.model.Chat;
import com.skripsi.biodev.model.RoomChat;
import com.skripsi.biodev.model.User;
import com.skripsi.biodev.util.SessionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.skripsi.biodev.SignInActivity.USER_SESSION;

public class ListChatActivity extends AppCompatActivity {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    ListChatAdapter adapter;

    FirebaseDatabase database;
    DatabaseReference ref;
    User user;
    List<RoomChat> roomChats = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_chat);
        ButterKnife.bind(this);

        adapter = new ListChatAdapter(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        user = SessionManager.getUser(this, USER_SESSION);

        database = FirebaseDatabase.getInstance();
        ref = database.getReference("chat");
        realTimeList();
    }

    private void realTimeList() {
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                roomChats.clear();
                for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {
                    if (isMine(chatSnapshot.getKey())) {
                        RoomChat roomChat = new RoomChat();

                        roomChat.roomId = chatSnapshot.getKey();
                        for (DataSnapshot childChatSnapshot : chatSnapshot.getChildren()) {
                            Chat chat = childChatSnapshot.getValue(Chat.class);
                            roomChat.lastMessage = chat.message;
                            roomChat.name = getReceiverName(chat.receiver_name, chat.sender_name);
                            roomChat.target_id = getReceiverId(chat.receiver_id, chat.sender_id);
                        }
                        roomChats.add(roomChat);
                    }
                }
                adapter.setItems(roomChats);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean isMine(String key) {
        int user0 = Integer.parseInt(key.split("-")[0]);
        int user1 = Integer.parseInt(key.split("-")[1]);
        return user0 == user.getData().getIdUser() || user1 == user.getData().getIdUser();
    }

    private int getReceiverId(int id, int id2) {
        if (id == user.getData().getIdUser()) {
            return id2;
        }
        return id;
    }

    private String getReceiverName(String n1, String n2) {
        if (n1.equals(user.getData().getNamed())) {
            return n2;
        }
        return n1;
    }
}
