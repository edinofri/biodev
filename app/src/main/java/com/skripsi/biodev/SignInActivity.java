package com.skripsi.biodev;

import android.view.View;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;
import com.medialablk.easytoast.EasyToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.skripsi.biodev.BuildConfig;
import com.skripsi.biodev.MainActivity;
import com.skripsi.biodev.R;
import com.skripsi.biodev.model.User;
import com.skripsi.biodev.network.APIService;
import com.skripsi.biodev.util.SessionManager;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.skripsi.biodev.util.AlertDialogManager.showAlertDialog;


import butterknife.BindView;

public class SignInActivity extends AppCompatActivity {

    private static final String TAG = SignInActivity.class.getSimpleName();

    @BindView(R.id.email)EditText inputEmail;
    @BindView(R.id.password)EditText inputPassword;

    private ProgressDialog progressDialog;

    private String email, password;

    private APIService apiService;

    public static void start(Context context) {
        Intent intent = new Intent(context, SignInActivity.class);
        context.startActivity(intent);
    }

    public static String USER_SESSION = "user_session";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_sign_in);

        if(isSessionLogin()) {
            MainActivity.start(this);
            SignInActivity.this.finish();
        }

        ButterKnife.bind(this);

        initComponents();

    }

    private void initComponents(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Harap tunggu...");
    }

    private void hideProgress() {
        if (progressDialog != null){
            progressDialog.dismiss();
        }
    }

    private void showProgress() {
        progressDialog.show();
    }

    private void getData(){
        email = inputEmail.getText().toString().trim();
        password = inputPassword.getText().toString().trim();
    }

    @OnClick(R.id.btn_login)
    public void doSignIn(){
        getData();

        authenticateUser(email, password);
    }

    private boolean isValidateForm(){
        boolean result = true;

        getData();

        if (TextUtils.isEmpty(email)) {
            inputEmail.setError("Masukkan email anda!");
            result = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            inputEmail.setError("Email yang anda masukkan tidak sesuai!");
            result = false;
        } else {
            inputEmail.setError(null);
        }

        if (TextUtils.isEmpty(password)) {
            inputPassword.setError("Masukkan password anda!");
            result = false;
        } else if (password.length() < 1){
            inputPassword.setError(getString(R.string.warning_minimum_password));
            result = false;
        } else {
            inputPassword.setError(null);
        }

        return result;
    }

    private void authenticateUser(final String email, String password){

        if (!isValidateForm()){
            return;
        }

        showProgress();

        apiService = new APIService();
        apiService.doLogin(email, password, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                User user = (User) response.body();
                hideProgress();
                if (user != null){
                    if (!user.isError()){
                        onSuccessLogin(SignInActivity.this, USER_SESSION, user);
                        MainActivity.start(SignInActivity.this);
                        SignInActivity.this.finish();
                        EasyToast.info(getApplicationContext(), "Welcome " + user.getData().getEmail());
                    } else {
                        showAlertDialog(SignInActivity.this, getString(R.string.app_name), getString(R.string.auth_failed), false);
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (BuildConfig.DEBUG)
                    Log.d(TAG + " -> onFailure: ", t.getMessage());
                EasyToast.error(getApplicationContext(), "Koneksi eror");
                hideProgress();
            }
        });
    }

    private void onSuccessLogin(Context context, String key, User user){
        SessionManager.putUser(context, key, user);
    }

    boolean isSessionLogin() {
        return SessionManager.getUser(this, USER_SESSION) != null;
    }


    public void kesignup(View view) {
        Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);startActivity(intent);
    }

}
