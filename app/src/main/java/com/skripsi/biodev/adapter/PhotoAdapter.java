package com.skripsi.biodev.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.skripsi.biodev.R;
import com.skripsi.biodev.model.Jurnal;

import uk.co.senab.photoview.PhotoViewAttacher;

import static com.skripsi.biodev.MainActivity.userName;
import static com.skripsi.biodev.util.Constant.URL_PICTURES;


public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoHolder> {

    private List<Jurnal> jurnalList;
    private Context context;
    private PhotoViewAttacher mAttacher;

    public static String urlPicture;
    public static String keterangan;
    public static int jurnalId;
    public static String kategoriId;

    public String[] mColors = {
            "#39add1", // light blue
    };

    public PhotoAdapter(Context context) {
        this.jurnalList = new ArrayList<>();
        this.context = context;
    }

    public void setDataAdapter(List<Jurnal> pictures){
        isLoading = false;
        if (pictures == null || pictures.size() == 0)
            return;
        if (jurnalList != null && jurnalList.size() > 0)
            this.jurnalList.clear();
        this.jurnalList = pictures;
        notifyDataSetChanged();
    }

    public List<Jurnal> getDataAdapter() {
        return jurnalList;
    }

    public void addDataAdapter(List<Jurnal> pictures) {
        isLoading = false;
        jurnalList.addAll(pictures);
        notifyDataSetChanged();
    }

    public class PhotoHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_view_date)TextView mTextViewDate;
        //@BindView(R.id.text_view_publisher)TextView mTextViewPublisher;
        @BindView(R.id.text_view_name)TextView mTextViewName;
        @BindView(R.id.text_view_category)TextView mTextViewCategory;
        @BindView(R.id.text_view_subcategory)TextView mTextViewSubCategory;
        @BindView(R.id.image_view_photo)ImageView mImageViewPhoto;
        public PhotoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo, parent, false);
        PhotoHolder photoHolder = new PhotoHolder(view);
        return photoHolder;
    }

    @Override
    public void onBindViewHolder(final PhotoHolder holder, int position) {
        final Jurnal picture = jurnalList.get(position);

        holder.mTextViewDate.setText(picture.getTglpublish());
        holder.mTextViewName.setText(picture.getJudul());
        //holder.mTextViewPublisher.setText(picture.getPublisherId());
        holder.mTextViewCategory.setText(picture.getKategori());
        holder.mTextViewSubCategory.setText(picture.getSubkategori());
        Glide
                .with(context)
                .load(URL_PICTURES + picture.getThumbnail())
                .into(holder.mImageViewPhoto);
        String firstCharUserName = userName.substring(0,1);
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(firstCharUserName, getColor());
    }

    @Override
    public int getItemCount() {
        return jurnalList != null ? jurnalList.size():0;
    }

    public int getColor() {
        String color;

        // Randomly select a fact
        Random randomGenerator = new Random(); // Construct a new Random number generator
        int randomNumber = randomGenerator.nextInt(mColors.length);

        color = mColors[randomNumber];
        int colorAsInt = Color.parseColor(color);

        return colorAsInt;
    }

    public boolean isLoading;

    public void setOnLoadMoreListener( RecyclerView recyclerView, final LoadMore listener) {
        final LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!isLoading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            listener.OnloadMoreListener();
                            isLoading = true;
                        }
                    }
                }
            }
        });
    }

    public interface LoadMore {
        void OnloadMoreListener();
    }
}
