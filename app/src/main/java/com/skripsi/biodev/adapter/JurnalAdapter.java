package com.skripsi.biodev.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.google.gson.Gson;
import com.skripsi.biodev.R;
import com.skripsi.biodev.fragment.JurnalDetFragment;
import com.skripsi.biodev.model.Jurnal;

import uk.co.senab.photoview.PhotoViewAttacher;

import static com.skripsi.biodev.MainActivity.userName;
import static com.skripsi.biodev.util.Constant.URL_PICTURES;


public class JurnalAdapter extends RecyclerView.Adapter<JurnalAdapter.JurnalHolder> {

    private List<Jurnal> jurnalList;
    private Context context;
    //List<Jurnal> itemJurnalList;

    private PhotoViewAttacher mAttacher;

    public static String urlPicture;
    public static String keterangan;
    public static int jurnalId;
    public static String kategoriId;

    public String[] mColors = {
            "#39add1", // light blue
    };

    public JurnalAdapter(Context context) {
        this.jurnalList = new ArrayList<>();
        this.context = context;
    }


    public void setDataAdapter(List<Jurnal> pictures) {
        isLoading = false;
        if (pictures == null || pictures.size() == 0)
            return;
        if (jurnalList != null && jurnalList.size() > 0)
            this.jurnalList.clear();
        this.jurnalList = pictures;
        notifyDataSetChanged();
    }

    public void addDataAdapter(List<Jurnal> pictures) {
        isLoading = false;
        jurnalList.addAll(pictures);
        notifyDataSetChanged();
    }
    public List<Jurnal> getDataAdapter() {
        return jurnalList;
    }


    public boolean isLoading;

    public void setOnLoadMoreListener(RecyclerView recyclerView, final PhotoAdapter.LoadMore listener) {
        final LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!isLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            listener.OnloadMoreListener();
                            isLoading = true;
                        }
                    }
                }
            }
        });
    }

    public class JurnalHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.text_view_date)
        TextView mTextViewDate;
        //@BindView(R.id.text_view_publisher)TextView mTextViewPublisher;
        @BindView(R.id.text_view_name)
        TextView mTextViewName;
        @BindView(R.id.text_view_category)
        TextView mTextViewCategory;
        @BindView(R.id.text_view_subcategory)
        TextView mTextViewSubCategory;
        @BindView(R.id.image_view_photo)
        ImageView mImageViewPhoto;

        public JurnalHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Jurnal jurnal = jurnalList.get(position);
            String extraJurnal = new Gson().toJson(jurnal, Jurnal.class);

            Bundle bundle = new Bundle();
            bundle.putString("Jurnal", extraJurnal);

            JurnalDetFragment jurnalDetFragment = new
                    JurnalDetFragment();
            jurnalDetFragment.setArguments(bundle);

            FragmentManager fm = ((Activity) context).getFragmentManager();
            fm.beginTransaction().replace(R.id.content_frame, jurnalDetFragment).addToBackStack("").commit();
        }
    }

    public JurnalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo, parent, false);
        return new JurnalHolder(itemView);
    }

    @Override
    public void onBindViewHolder(JurnalAdapter.JurnalHolder holder, int position) {
        final Jurnal jurnalItem = jurnalList.get(position);

        jurnalItem.getIdartikel();
        holder.mTextViewDate.setText(jurnalItem.getTglpublish());
        holder.mTextViewName.setText(jurnalItem.getJudul());
        //holder.mTextViewPublisher.setText(picture.getPublisherId());
        holder.mTextViewCategory.setText(jurnalItem.getKategori());
        holder.mTextViewSubCategory.setText(jurnalItem.getSubkategori());
        Glide
                .with(context)
                .load(URL_PICTURES + jurnalItem.getThumbnail())
                .into(holder.mImageViewPhoto);
        String firstCharUserName = userName.substring(0, 1);
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(firstCharUserName, getColor());
    }

    @Override
    public int getItemCount() {
        return jurnalList != null ? jurnalList.size() : 0;
    }

    public int getColor() {
        String color;

        // Randomly select a fact
        Random randomGenerator = new Random(); // Construct a new Random number generator
        int randomNumber = randomGenerator.nextInt(mColors.length);

        color = mColors[randomNumber];
        int colorAsInt = Color.parseColor(color);

        return colorAsInt;
    }
}
