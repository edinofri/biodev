package com.skripsi.biodev.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import com.skripsi.biodev.MainActivity;
import com.skripsi.biodev.model.Jurnal;

import static com.skripsi.biodev.util.Constant.URL_PICTURES;


public class JurnalProfileAdapter extends BaseAdapter {

    private ArrayList<Jurnal> pictureList;
    private Context context;

    public static String urlPicture;

    public JurnalProfileAdapter(Context context) {
        this.pictureList = new ArrayList<>();
        this.context = context;
    }

    public void setDataAdapter(List<Jurnal> pictures){
        if (pictures == null || pictures.size() == 0)
            return;
        if (pictureList != null && pictureList.size() > 0)
            this.pictureList.clear();
        this.pictureList.addAll(pictures);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return pictureList != null ? pictureList.size():0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView;
        if (view == null) {
            // If the view is not recycled, this creates a new ImageView to hold an image
            imageView = new ImageView(context);
            // Define the layout parameters
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new GridView.LayoutParams(120, 120));
        } else {
            imageView = (ImageView) view;
        }

        // Set the image resource and return the newly created ImageView
        final Jurnal picture = pictureList.get(i);
        Glide
                .with(context)
                .load(URL_PICTURES + picture.getThumbnail())
                .into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                urlPicture = picture.getThumbnail();
            }
        });
        return imageView;
    }
}
