package com.skripsi.biodev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skripsi.biodev.R;
import com.skripsi.biodev.model.Chat;
import com.skripsi.biodev.model.User;
import com.skripsi.biodev.util.SessionManager;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.skripsi.biodev.SignInActivity.USER_SESSION;

/**
 * Created by tukangbasic on 24/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Chat> items = new ArrayList();
    Context context;

    final int TYPE_LEFT = 1;
    final int TYPE_RIGHT = 2;

    public ChatAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Holder view = null;
        switch (viewType) {
            case TYPE_LEFT:
                view = new Holder(LayoutInflater.from(context).inflate(R.layout.item_chat_left, parent, false));
                break;
            case TYPE_RIGHT:
                view = new Holder(LayoutInflater.from(context).inflate(R.layout.item_chat_right, parent, false));
                break;
        }
        return view;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
        Holder holder = (Holder) h;
        holder.txtMesssage.setText(items.get(position).message);
        holder.txtTime.setText(items.get(position).date);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        Chat chat = items.get(position);
        User user = SessionManager.getUser(context, USER_SESSION);
        int user_id = user.getData().getIdUser();
        if (chat.sender_id == user_id) {
            return TYPE_RIGHT;
        }
        return TYPE_LEFT;
    }

    public void setItems(List<Chat> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.message)
        TextView txtMesssage;
        @BindView(R.id.time)
        TextView txtTime;

        public Holder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
