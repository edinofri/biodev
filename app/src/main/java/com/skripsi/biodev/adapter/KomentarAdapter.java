package com.skripsi.biodev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skripsi.biodev.R;
import com.skripsi.biodev.model.Komen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tukangbasic on 23/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */

public class KomentarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Komen> items = new ArrayList<>();
    Context context;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new Holder(LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
        Holder holder = (Holder)h;
        Komen komen = items.get(position);
        holder.txtComment.setText(komen.getKomennya());
        holder.txtName.setText(komen.getNama());
        holder.txtTanggal.setText(komen.getTanggal());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<Komen> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.tanggal)
        TextView txtTanggal;
        @BindView(R.id.name)
        TextView txtName;
        @BindView(R.id.comment)
        TextView txtComment;
        public Holder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }
    }
}
