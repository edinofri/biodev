package com.skripsi.biodev.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skripsi.biodev.ChatActivity;
import com.skripsi.biodev.R;
import com.skripsi.biodev.model.RoomChat;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tukangbasic on 24/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */

public class ListChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public void setItems(List<RoomChat> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    List<RoomChat> items = new ArrayList<>();

    Context context;

    public ListChatAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(context).inflate(R.layout.item_chat_list, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
        Holder holder = (Holder)h;
        holder.txtName.setText(items.get(position).name);
        holder.txtLastMessage.setText(items.get(position).lastMessage);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView txtName;
        @BindView(R.id.last_message)
        TextView txtLastMessage;

        public Holder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra("receiver_id", items.get(getAdapterPosition()).target_id);
                    intent.putExtra("receiver_name", items.get(getAdapterPosition()).name);
                    context.startActivity(intent);
                }
            });
        }
    }
}
